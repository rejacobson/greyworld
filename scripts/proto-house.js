define(['jquery', 'lib/handlebars', 'lib/structure/house', 'app/region/scene', 'lib/terrain'], function($, Handlebars, House, Scene, Terrain) {

  Terrain.initialize(123);

  //the jquery.alpha.js and jquery.beta.js plugins have been loaded.
  $(function() {
  
    var scene = new Scene([0, 0]);
    scene.generate();
    scene.show();

    var structures = [];
    structures.push(House.generate('seed', 6, 3));
    structures.push(House.generate('seed', 3, 3));
    structures.push(House.generate('seed', 4, 6));

    for (var i=0; i<15; ++i) {
      structures.push(House.generate('seed', 7, 5));
    }
    
    var $scenery = scene.scenery();

    _.each(structures, function($structure) {
      var $wrapper = $('<div class="structure"></div>');
      $scenery.append($structure.hide());
      $wrapper.width($structure.width());
      $structure.wrap($wrapper).show();
    });
    
  });

});
