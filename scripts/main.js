require(["jquery", "app/game"], function($, Game) {

  //the jquery.alpha.js and jquery.beta.js plugins have been loaded.
  $(function() {

    Game.start();

    console.log('Initialized the project with jQuery');

  });

});
