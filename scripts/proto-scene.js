define(['jquery', 'underscore', 'app/region/scene', 'lib/terrain', 'app/hud', 'lib/input'],
function($, _, Scene, Terrain, Hud, Input) {

  Terrain.initialize(123);

  var PlaySession = function() {
    this.position = [0, 0];
  };


  var Minimap = function(session, width, height) {
    if (width % 2 == 0) width++;
    if (height % 2 == 0) height++;    

    var $html = $('<div>')
          .attr('id', 'minimap')
          .addClass('info')
          .append('<table>');

    this.update = function(x, y) {
      $('table', $html).empty();

      var x = session.position[0] -  

      for (var y=0; y<width; ++y) {
        var tr = $('<tr>'), td;
        for (var x=0; x<height; ++x) {
          td = $('<td>');
           
        }
        $('table', $html).append(tr);
      }

    }

    this.html = function() {

    }  
  };

  var SceneList = function() {
    this.scenes = {};
    this.current_scene;
    this.max = 10;

    this.getByCoords = function(coords) {
      var index = Scene.stringifyCoords(coords);
      return this.getByIndex(index);
    }

    this.getByIndex = function(index) {
      return this.scenes[index]; 
    }

    this.current = function(scene) {
      if (scene) {
        this.current_scene = scene;
        scene.show();
      }
      return this.current_scene;
    }

    this.add = function(scene) {
      var index = Scene.stringifyCoords(scene.coords()); 
      this.scenes[index] = scene;
      if (_.keys(this.scenes).length > this.max) {
        this.remove(this.scenes[_.first(_.keys(this.scenes))]);
      }
      return this;
    }

    this.remove = function(scene) {
      var index = Scene.stringifyCoords(scene.coords());
      this.scenes[index].remove();
      delete this.scenes[index];
      return this;
    }

    this.refresh = function(scene) {
      var index = Scene.stringifyCoords(scene.coords());
      if (this.scenes[index]) {
        this.scenes[index] = null;
        delete this.scenes[index];
        this.scenes[index] = scene;
      }
      return this;
    }

    this.toString = function() {
      var str = []
      _.each(this.scenes, function(scene, index) {
        str.push('+ '+ index +' => '+ scene.id() +' -- '+ scene.html().attr('id'));
      }); 
      return str.join("\n");
    }
  };

  var World = function(play_session){
    this.scenes = new SceneList();
    this.session = play_session;
  }

  _.extend(World.prototype, {
    
    loadScene: function(coords) {
      var scene = this.scenes.getByCoords(coords);

      if (!scene) {
        scene = new Scene(coords);
        this.scenes.add(scene);
      } else {
        this.scenes.refresh(scene);
      }
      return scene;
    },

    loadAdjacentScenes: function(coords) {
      var x = coords[0], y = coords[1];
      this.loadScene([x+1, y]);
      this.loadScene([x-1, y]);
      this.loadScene([x, y+1]);
      this.loadScene([x, y-1]);
    },

    currentScene: function() {
      return this.scenes.current();
    },

    gotoScene: function(coords) {
      this.loadAdjacentScenes(coords);
      this.scenes.current(this.loadScene(coords));
    }

  });

  $(document).ready(function() {
    var session, world;

    $(document).on('scene.show', function(e, scene) {
      var biome = Terrain.biome(session.position[0], session.position[1]);
      Hud.location(biome.name + (biome.edge ? ' / '+ biome.edge : '') +' -- '+ session.position[0] +':'+ session.position[1]);
    });

    setInterval(function(){ Input.update(); }, 50);

    Input.on('keyup.w', function(){
      session.position[1] -= 1;
      world.gotoScene(session.position);
    }).on('keyup.s', function(){
      session.position[1] += 1;
      world.gotoScene(session.position);
    }).on('keyup.a', function(){
      session.position[0] -= 1;
      world.gotoScene(session.position);
    }).on('keyup.d', function(){
      session.position[0] += 1;
      world.gotoScene(session.position);
    });

    session = new PlaySession();
    world = new World(session);
    world.gotoScene(session.position);

  });

});

