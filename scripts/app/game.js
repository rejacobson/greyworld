define(['jquery', 'underscore', 'lib/input', './region/world', './entity/mob', './entity/tree'], function($, _, Input, World, Mob, Tree) {

  var player = new Mob('/images/player.png');
  player.stats({speed: 300});
  player.position(200, 10);

  var tree = new Tree('oak', {
    trunk: [20, 200],
    foliage: [80, 150]
  });
  tree.position(800, 50);

//console.log(World);
//window.World = World;



/*
  var cat = Mob.create('Cat', {
    position: [],
    image: '',
    stats: {
      hp: 100,
      str: 5,
      int: 4
    }
  });

  var tree = Static.create('Tree', {});

  var sword = Item.create('Sword', {});

  var cat = new Entity([5, 5], '/images/cat.png');
  var dog = new Entity([5, 5], '/images/dog.png');

  window.cat = cat;
  window.dog = dog;
*/

  var requestAnimationFrame = (function(){
    //Check for each browser
    //@paul_irish function
    //Globalises this function to work on any browser as each browser has a different namespace for this
    try{
      window;
    } catch(e){
      return;
    }
    
    return window.requestAnimationFrame || //Chromium
           window.webkitRequestAnimationFrame || //Webkit
           window.mozRequestAnimationFrame || //Mozilla Geko
           window.oRequestAnimationFrame || //Opera Presto
           window.msRequestAnimationFrame || //IE Trident?
           function(callback, element) { //Fallback function
             window.setTimeout(callback, 1000/60);
           }
       
  })();

  var pageHiddenProperty = (function(){
    var prefixes = ['webkit','moz','ms','o'];
    
    // if 'hidden' is natively supported just return it
    if ('hidden' in document) return 'hidden';
    
    // otherwise loop over all the known prefixes until we find one
    for (var i = 0; i < prefixes.length; i++){
      if ((prefixes[i] + 'Hidden') in document) return prefixes[i] + 'Hidden';
    }

    // otherwise it's not supported
    return null;
  })();

  function pageIsHidden() {
    if (!pageHiddenProperty) return false;
    return document[pageHiddenProperty];
  };

  function onVisibilityChange(hidden_callback, visible_callback) {
    if (pageHiddenProperty) {
      var eventname = pageHiddenProperty.replace(/[H|h]idden/, '') + 'visibilitychange';
      document.addEventListener(eventname, function(event){
        if (pageIsHidden()) {
          if (hidden_callback && _.isFunction(hidden_callback)) hidden_callback(event);
        } else {
          if (visible_callback && _.isFunction(visible_callback)) visible_callback(event);
        }
      });
    }
  };

  var Game = function() {
    var world = new World();

    world.scene().addEntity(player);
    world.scene().addEntity(tree);

    Input.on('keydown.w', function(){
      player.moving([0, -1]);
    }).on('keydown.s', function(){
      player.moving([0, 1]); 
    }).on('keydown.a', function(){
      player.moving([-1, 0]); 
    }).on('keydown.d', function(){
      player.moving([1, 0]); 
    });

    Input.on('keyup.w', function(){
      player.stopping([0, -1]);
    }).on('keyup.s', function(){
      player.stopping([0, 1]);
    }).on('keyup.a', function(){
      player.stopping([-1, 0]);
    }).on('keyup.d', function(){
      player.stopping([1, 0]);
    });

    var paused = false,
        dt, last_t; 

    // Game loop
    // Update entities
    this.start = function() {
      tick(0);
    } 

    this.pause = function() {
      paused = true;
    }

    this.unpause = function() {
      paused = false;
      last_t = (new Date()).getTime() - msDuration;
      tick();
    }

    function tick(t) {
      t = t || (new Date()).getTime();
      dt = (t - last_t) * 0.001; // number of seconds since last update
      last_t = t;

      Input.update(dt);
      world.update(dt);

      if (!paused) requestAnimationFrame(tick);
    }

    var self = this;
    onVisibilityChange(function(){
      self.pause(); 
    }, function(){
      self.unpause();
    });
  };
 
  return new Game();

});
