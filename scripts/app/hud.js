define(['jquery', 'underscore', 'text!/templates/hud.html'], function($, _, Template) {

  var $html = $(Template);

  $(document).ready(function(){
    $('body').append($html);
  });

  var Hud = {

    location: function(loc) {
      $('.location', $html).html(loc);
    }

  };
 
  return Hud;

}); 
