define(['jquery', 'underscore', 'lib/spatialpartition', 'lib/handlebars', 'text!/templates/scene.html', 'lib/generator', 'lib/terrain'],
function($, _, Grid, Handlebars, Template, Generator, Terrain) {

  //                   [0, 0]
  var Scene = function(coords) {
    var _coords = coords.slice(0);

    this.coords = function() { return _coords.slice(0); }

    this.$html = $(Template)
      .attr('id', 'scene-'+ Scene.stringifyCoords(_coords))
      .data('scene', this)
      .hide()
      .appendTo('body');

    var biome = Terrain.biome(coords[0], coords[1]);

    this.$html.addClass(biome.name).data('biome', biome.name);
    if (biome.edge) this.$html.addClass(biome.edge +'-edge').data('biome-edge', biome.edge);

    this.partition = new Grid([this.road().width(), this.road().height()], [100, 100]);
    this.entities = [];
  };

  Scene.stringifyCoords = function(coords) {
    return coords.join('-');
  };

  // Prototype
  _.extend(Scene.prototype, {

    id: function() {
      return this.$html.attr('id');
    },

    html: function() {
      return this.$html;
    },

    width: function() {
      return this.$html.width();
    },

    road: function() {
      return $('.road', this.$html);
    },

    scenery: function() {
      return $('.scenery', this.$html);
    },

    background: function() {
      return $('.background', this.$html);
    },

    curb: function() {
      return $('.curb', this.$html);
    },

    show: function() {
      var self = this;
      $('body > .scene').filter(':visible').each(function(){
        var $this = $(this);
        if ($this.attr('id') != self.id()) $this.data('scene').hide();
      });
      this.$html.show();
window.current_scene = this;
      $(document).trigger('scene.show', [this]);
    },

    hide: function() {
      this.$html.hide();
      $(document).trigger('scene.hide', [this]);
    },

    remove: function() {
      this.$html.remove();
      $(document).trigger('scene.remove', [this]);
    },

    addEntity: function(entity) {
      if (_.indexOf(this.entities, entity) >= 0) return;
      
      var index;
      
      // Try to re-use an existing empty spot in the array
      if ((index = _.indexOf(this.entities, undefined)) >= 0) {
        this.entities[index] = entity;
        entity.__pindex = index;
      } else {
        this.entities.push(entity);
        entity.__scene_index = this.entities.length - 1;
      }

      // Insert the entity into the spatial partition
      index = this.partition.getIndexByPosition(entity.position);
      this.reindexEntity(index, entity);

      // Add the entity to the dom
      this.road().append(entity.html());
    },

    addScenery: function($scenery) {
      this.scenery().append($scenery);
    },
    
    reindexEntity: function(index, entity) {
      if (index == entity.__partition_index) return;
      if (entity.__partition_index) this.partition.remove(entity.__partition_index, entity);
      entity.__partition_index = index;
      this.partition.insert(index, entity);
    },
    
    removeEntity: function(entity) {
      if (entity.__scene_index) this.entities[entity.__scene_index] = undefined;
      if (entity.__partition_index) this.partition.remove(entity.__partition_index, entity); 
      delete entity.__scene_index;
      delete entity.__partition_index;
      
      // Remove the entity from the dom
      entity.html().remove();
    },
    
    // Dynamically generate the scene contents
    // Maybe this should be moved to a generator object and passed in
    generate: function() {
console.log('#generate');
      //Generator.create_scene(this); 
    },

    update: function(dt) {
      var entity, pos, road = this.road();

      for (var i=0, len=this.entities.length; i<len; ++i) {
        entity = this.entities[i];

        if (entity.update) {
          entity.update(dt); 

          pos = entity.position();

          if (pos[1] < 0) entity.position(pos[0], 0, pos[2]);
          else if (pos[1] > road.height()) entity.position(pos[0], road.height(), pos[2]);

          if (pos[0] < 0) entity.position(0, pos[1], pos[2]);
          else if (pos[0] > road.width()) entity.position(road.width(), pos[1], pos[2]);
        }
      }
    }

  }); // end prototype extend

  return Scene;

});
