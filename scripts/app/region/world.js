define(['jquery', 'underscore', './scene'], function($, _, Scene) {

  var World = function() {
    this.max_scenes = 10;
    this.scenes = {};
    this.current_scene = [0, 0];

    this.players = [];

    this.addScene(new Scene([0, 0])).show();
  };

  _.extend(World.prototype, {

    addPlayer: function(player) {
      this.players.push(player);
      
      var scene = this.scene();
      if (scene) scene.addEntity(player);
    },
    
    stringifyCoords: function(coords) {
      return coords.join(':');
    },
    
    scene: function() {
      return this.getScene(this.current_scene);
      var index = this.stringifyCoords(this.current_scene);
      return this.scenes[index];
    },

    getScene: function(coords) {
      var index = this.stringifyCoords(coords);
      if (!this.scenes[index]) {
        var scene = new Scene(coords);
        scene.generate();
        this.scenes[index] = scene;
      }
      return this.scenes[index];
    },

    createScene: function(coords) {
      return new Scene(coords, 2000);
    },

    addScene: function(scene) {
      var index = this.stringifyCoords(scene.coords);
      if (this.scenes[index]) return;
      this.scenes[index] = scene;
      return scene;
    },

    visitScene: function(coords) {
      index = this.stringifyCoords(coords);

      if (!this.scenes[index]) {
        var scene = new Scene(coords, 2000);
        this.addScene(scene);
      }

      var current_scene = this.scene(),
          new_scene = this.scenes[index];

      _.each(this.players, function(player) {
        current_scene.removeEntity(player);
        new_scene.addEntity(player); 
      });
      
      this.current_scene = coords;
      this.scene().show();
    },

    update: function(dt) {
      var keys = _.keys(this.scenes);
      for (var i=0, len=keys.length; i<len; ++i) {
        this.scenes[keys[i]].update(dt);
      }     
    }

  }); // end prototype extend

  return World;

});
