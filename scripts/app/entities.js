define(['app/entity/base', 'app/entity/mob', 'app/entity/tree'],
function(Base, Mob, Tree) {

  return {
    Base: Base,
    Mob: Mob,
    Tree: Tree
  };

});
