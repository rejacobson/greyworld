define(['jquery', 'underscore', 'lib/EventEmitter', 'text!/templates/entity/base.html'], function($, _, E, Template) {

  var Avatar = {

    html: function(src) {
      if (!this.$html || src != undefined) {
        this.$html = $(Template).attr('id', 'entity-'+ this.id()).data('entity', this);
         
        if (src) {
          // Image path
          if (typeof(src) == 'string') {
            var self = this;
            $('<img>').attr('src', src).load(function(){
              var w = this.width, h = this.height;
              self.$html.css({width: w});
              $('.image', self.$html).css({backgroundImage: 'url('+ src +')', width: w, height: h});
            });

          // jQuery object
          } else {
            // Must be added to the DOM to figure out it's width
            var width = src.hide().appendTo('body').width();
            src.remove().show().addClass('image');
            $('.image', this.$html).replaceWith(src);
            this.$html.css({width: width});
          }
        }
      }

      return this.$html;
    },

    avatarId: function() {
      return this.html().attr('id');
    },

    removeAvatar: function() {
      if (this.$html) this.$html.remove();
    },

    attach: function($element) {
      var avatar = this.html();

      if ($element.hasClass('entity')) {
        var dx = $element.css('left') - avatar.css('left'),
            dy = $element.css('top') - avatar.css('top');
    
        $element.css({left: dx, top: dy});
      }

      avatar.append($element);

      E.trigger($element.attr('id') +'.attached', this);
    },

    attachTo: function($element) {
      var avatar = this.html();

      if ($element.hasClass('entity')) {
        $element.data('entity').attach(avatar); 
      } else {
        $element.append(avatar);
      }
    }

  };

  return Avatar;

});
