define(function() {

  return {
    tags: function() {
      if (!this._tags) this._tags = {};
      return this._tags;
    },

    addTag: function(name, value) {
      var tags = this.tags();

      if (_.isObject(name)) {
        _.extend(tags, name);
      } else {
        tags[name] = value || true;
      }
    },

    removeTag: function(name) {
      var tags = this.tags();
      delete tags[name];
    },

    hasTag: function(name) {
      var tags = this.tags();
      return tags[name] ? tags[name] : false;
    }
    
  };

});
