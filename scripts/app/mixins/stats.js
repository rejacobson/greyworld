define(function() {

  return {

    // Get or set all of the stats
    stats: function(def) {
      if (!this._stats) {
        this._base_stats = {};
        this._stats = {};
      }
      if (_.isObject(def)) {
        _.extend(this._base_stats, def);
        this._stats = _.clone(this._base_stats);
      }
      return this._stats;
    },

    // Get or set a single stat
    stat: function(name, value) {
      var stats = this.stats();
      if (value !== undefined) stats[name] = value;
      return stats[name];
    },

    // Get the base stats; read-only
    baseStats: function() {
      var stats = this.stats();
      return _.clone(this._base_stats);
    },

    // Get a single base_stat
    baseStat: function(name) {
      var stats = this.stats();
      return this._base_stats[name];
    },

    // Reset all of the stats to the base_stats
    resetStats: function() {
      var stats = this.stats(), base = this._base_stats;
      stats = _.clone(base);
    },

    // Reset one stat back to it's base_stat
    resetStat: function(name) {
      var stats = this.stats(), base = this._base_stats;
      stats[name] = base[name];
    },

    // All current stats become the new base_stats
    saveStats: function() {
      var stats = this.stats(), base = this._base_stats;
      base = _.clone(stats);
    },

    // One stat becomes the new base_stat
    saveStat: function(name) {
      var stats = this.stats(), base = this._base_stats;
      base[name] = stats[name];
    }
  };

});

