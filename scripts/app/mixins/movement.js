define(['underscore'], function(_){

  var Movement = {
    moving: function(x, y, z) {
      var speed = this.stat('speed'),
          self = this;

      if (!speed) return;

      _.each(_.isArray(x) ? x : arguments, function(val, index){
        if (val != 0) self._velocity[index] = speed * val;
      });
    },

    stopping: function(x, y, z) {
      var self = this;

      _.each(_.isArray(x) ? x : arguments, function(val, index){
        if (val != 0) {
          if ((val > 0 && self._velocity[index] > 0) || (val < 0 && self._velocity[index] < 0)) self._velocity[index] = 0;
        }
      });
    }
  }; 

  return Movement;

});
