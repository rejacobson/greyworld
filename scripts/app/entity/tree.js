define(['underscore', 'lib/inheritance', 'app/entity/base', 'text!/templates/entity/tree.html'], function(_, Class, Entity, Template) {

  /**
   *
   * Variables: type, trunk width/height, foliage width/height

      var tree = new Tree('oak', {
        trunk: [30, 200],
        foliage: [200, 200]
      });

   */
  var Tree = Class.extend(Entity, {

    initialize: function(type, options) {
      var html = Tree.html(type, options);
      this.parent(html);
    } 

  });

  Tree.html = function(type, options) {
    var $html = $(Template).addClass(type);
    if (options.trunk) $html.css({width: options.trunk[0], height: options.trunk[1]});
    if (options.foliage) $('.foliage', $html).css({width: parseInt(options.foliage[0]/2), height: options.foliage[1]});
    return $html; 
  };

  Tree.generate = function(Random) {
    var trunk = [Random.range(5, 20), Random.range(100, 300)], 
        foliage = [trunk[0] * 5, trunk[1] * 0.75];

    return new Tree('oak', {
      trunk: trunk,
      foliage: foliage 
    });
  };

  return Tree;

});
