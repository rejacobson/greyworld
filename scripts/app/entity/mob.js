define(['underscore', 'lib/inheritance', 'app/entity/base', 'app/mixins/movement'],
function(_, Class, Entity, Movement){

  var Mob = Class.extend(Entity, {
    include: [Movement]
  });

  return Mob;

});
