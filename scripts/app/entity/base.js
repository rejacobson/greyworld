define(['underscore', 'lib/inheritance', 'lib/vector', 'app/mixins/tags', 'app/mixins/stats', 'app/mixins/avatar'],
function(_, Class, V, Tags, Stats, Avatar){

  var __id = 0;

  var Entity = Class.extend({

    include: [Tags, Stats, Avatar],

    initialize: function(image) {
      // Private
      var _id = ++__id;

      // Public
      this._position = [0, 0, 0]; // [x, y, altitude]
      this._last_position = _.clone(this._position);
      this._velocity = [0, 0, 0];

      this.id = function() { return _id; }

      // Initialization
      this.html(image);
    },

    position: function(x, y, z) {
      if (arguments.length) {
        // Save the last position
        this._last_position[0] = this._position[0];
        this._last_position[1] = this._position[1];
        this._last_position[2] = this._position[2];

        // Update the position
        if (_.isArray(x)) {
          this._position[0] = x[0];
          this._position[1] = x[1];
          this._position[2] = (x[2] != undefined) ? x[2] : this._position[2];
        } else {
          this._position[0] = x;
          this._position[1] = y;
          this._position[2] = (z != undefined) ? z : this._position[2];
        }

        // Update the avatar with the new position
        if (this.has_moved()) {
          _.extend(this.html()[0].style, {
            left:   Math.round(this._position[0]) +'px',
            top:    Math.round(this._position[1] - this._position[2]) +'px',
            zIndex: Math.round(this._position[1] + this._position[2])
          });
        }
      }

      return this._position;
    },

    has_moved: function() {
      return (this._last_position[0] != this._position[0] ||
              this._last_position[1] != this._position[1] ||
              this._last_position[2] != this._position[2]);
    },

    update: function(dt) {
      // Apply velocity to the position
      if (V.len_sq(this._velocity) != 0) {
        this.position(this._position[0] + this._velocity[0] * dt,
                      this._position[1] + this._velocity[1] * 0.5 * dt,
                      this._position[2] + this._velocity[2] * dt);
      }
    }

  });

  return Entity;

});
