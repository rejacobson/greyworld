define(['underscore', 'lib/simplex-noise', 'lib/mersenne', 'json!/json/biome/climate_table.json', 'json!/json/biome/conversion_table.json'],
function(_, SimplexNoise, MersenneTwister, ClimateTable, ConversionTable) {
/*
bare
deciduous
desert
grassland
marsh
savanna
scorched
shrub
snow
taiga
temperate
tropical
tundra
water

                                          Moisture

                  6          5          4          3          2          1
Elevation     

         |                         |          |         |            |           |
 5       |          snow           |  tundra  |   bare  |  scorched  | scorched  |
         |                         |          |         |            |           |
         |                         |                                 |           |
 4       |          Taiga          |                shrub            |   bare    |
         |                         |                                 |           |
         |   Temperate  |     Deciduous       |                                  |
 3       |  Rain Forest |       Forest        |             Grassland            |
         |              |                     |                                  |
         |              |      Tropical       |                      |           |
 2       |    Swamp     |    Rain Forest      |       Savanna        |   Desert  |
         |              |                     |                      |           |
         |
 1       |                                  Water
         |   
     

*/

  var Tables = {
    Elevation: {
      simplex: null,
      resolution: 0.01
    },
    Moisture: {
      simplex: null,
      resolution: 0.02
    },
    Habitation: {
      simplex: null,
      resolution: 0.05
    }
  };


   
  var initialized = false;

  var initialize = function(seed) {
    if (!seed) seed = Math.random() * 10000000;
    var Rng = new MersenneTwister(seed);

    _.each(Tables, function(values, type) {
      values.simplex = new SimplexNoise(new MersenneTwister(Rng.range(1, 100000000))); 
    });

    initialized = true;
  }; 

  var scores = function(x, y) {
    var _scores = {};

    _.each(Tables, function(values, type) {
      _scores[type] = score(type, x, y); 
    });

    return _scores;
  }; 

  var score = function(type, x, y) {
    if (!Tables[type]) throw new Error('There is no Terrain Table of type: '+ type);
    if (!initialized) initialize(); 

    var table = Tables[type],
        _score = table.simplex.noise2D(x * table.resolution, y * table.resolution);

    _score = ((_score*100) + 100) * 0.5;
    _score = _score.toPrecision(5);

    return _score;
  };

  var getBiome = function(moisture, elevation) {
    return _.intersection(ClimateTable.Moisture[moisture], ClimateTable.Elevation[elevation])[0];
  };

  var biomeData = function(type, x, y) {
    var _score = score(type, x, y),
        table = ConversionTable[type],
        level,
        density,
        edge = 0,
        edge_threshold = 100 / ConversionTable[type].length / 3.75;

    for (var i=0, len=table.length; i<len; ++i) {
      level = i + 1;

      if (_score < table[i]) {
        var hi = table[i],
            lo = i == 0 ? 0 : table[i-1];

        // Near the next higher climate level?
        if (_score > hi - edge_threshold) {
          edge = level + 1; 
        } 

        // Near the next lower climate level?
        else if (_score < lo + edge_threshold) {
          edge = level - 1;
        }

        var range = hi - lo,
            mid = lo + (range / 2);

        density = ((mid-lo) - Math.abs(mid - _score)) / (mid - lo);

        break;
      }
    }

    return {
      level: level,
      score: _score, 
      edge: edge,
      density: density
    };
  };

  var biome = function(x, y) {
    var M = biomeData('Moisture', x, y),
        E = biomeData('Elevation', x, y);
    
    return {
      name: getBiome(M.level, E.level),
      edge: (M.edge && E.edge) ? getBiome(M.edge, E.edge) : null,
      density: (M.density + E.density) / 2,
      moisture: M.score,
      elevation: E.score
    };
  };

/*
  var biome = function(x, y) {
    var M_score = score('Moisture', x, y),
        E_score = score('Elevation', x, y),
        M_table = ConversionTable.Moisture,
        E_table = ConversionTable.Elevation,
        M, E,
        M_edge, E_edge,
        M_density, E_density,
        edge_biome, edge_threshold = 4;

    edge_M = 0;
    for (var i=0, len=M_table.length; i<len; ++i) {
      M = i + 1;

      if (M_score < M_table[i]) {
        var hi = M_table[i],
            lo = i == 0 ? 0 : M_table[i-1];

        // Near the next higher climate level?
        if (M_score > hi - edge_threshold) {
          M_edge = M + 1; 
        } 

        // Near the next lower climate level?
        else if (M_score < lo + edge_threshold) {
          M_edge = M - 1;
        }

        var range = hi - lo,
            mid = lo + (range / 2);

        M_density = ((mid-lo) - Math.abs(mid - M_score)) / (mid - lo);

        break;
      }
    }

    for (var i=0, len=E_table.length; i<len; ++i) {
      E = i + 1;

      if (E_score < E_table[i]) {
        var hi = E_table[i],
            lo = i == 0 ? 0 : E_table[i-1];

        // Near the next higher climate level?
        if (E_score > hi - edge_threshold) {
          E_edge = E + 1; 
        } 

        // Near the next lower climate level?
        else if (E_score < lo + edge_threshold) {
          E_edge = E - 1;
        }

        var range = hi - lo,
            mid = lo + (range / 2);

        E_density = ((mid-lo) - Math.abs(mid - E_score)) / (mid - lo);

        break;
      }
    }

    return {
      name: getBiome(M, E),
      edge: (M_edge && E_edge) ? getBiome(M_edge, E_edge) : null,
      density: (M_density + E_density) / 2,
      moisture: M_score,
      elevation: E_score
    };
  }; 
*/

  return {
    initialize: initialize,
    scores: scores,
    score: score,
    biome: biome
  };

});

