define(['underscore'], function(_){

  //+ Jonas Raoni Soares Silva
  //@ http://jsfromhell.com/array/shuffle [v1.0]
  var shuffle = function(o, random){ //v1.0
    random = random || Math;
    for(var j, x, i = o.length; i; j = parseInt(random.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
    return o;
  };

  var probability_map = function(items, random) {
    var map = [];
    _.each(items, function(count, name) {
      for (var i=0; i<count; ++i) {
        map.push(name);
      }
    });

    return shuffle(map, random || Math);
  } 

  return {
    probability_map: probability_map,
    shuffle: shuffle
  };

});
