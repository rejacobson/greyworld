define(['underscore', 'lib/mersenne', './house/templates', './house/storey', './house/roof'], function(_, MersenneTwister, Templates, Storey, Roof) {

  var Random = new MersenneTwister();

  /**
   * Add features to a storey
   * Doors, windows, balconys, etc
   */
  function create_features($storey) {
    var sections = $('section', $storey);
    // Ground floor
    if ($storey.data('level') == 1) {
      
    } 
  }

  // House object
  var House = function() {
    this.$html = $(Templates.house()); 
  };

  // House prototype
  _.extend(House.prototype, {

    html: function() {
      return this.$html;
    }

  });

  // House class methods
  House.generate = function(seed, max_width, max_height) {
    var house = new House(),
        floors = Random.range(1, max_height),
        house_width = Random.range(2, max_width),
        floor_width,
        floor_height;
    
    var floor_width = house_width;
  
    var storeys = [];
    for (var i=1; i<=floors; ++i) {
      var storey = Storey.create(i, floor_width, Random);
      floor_width = storey.width;
      storeys.unshift(storey);
    }

    storeys.unshift(Roof.create(floor_width, Random));

    var contents = '';
    _.each(storeys, function(storey) {
      contents = $('<div>').addClass('storey-wrapper').append(storey.html).prepend(contents);
      if (storey.roofing && storey.roofing[0]) contents.addClass('P'+ storey.height);
    });

    house.html().append(contents);

    return house.html();
  };

  return House;

});
