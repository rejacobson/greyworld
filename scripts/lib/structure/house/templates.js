define(['lib/handlebars',
        'text!/templates/structure/house/house.html',
        'text!/templates/structure/house/storey.html',
        'text!/templates/structure/house/section.html',
        'text!/templates/structure/house/roof-center.html',
        'text!/templates/structure/house/roof-left.html',
        'text!/templates/structure/house/roof-right.html',
        'text!/templates/structure/house/feature.html',
        'text!/templates/structure/house/door.html',
        'text!/templates/structure/house/window.html',
        'text!/templates/structure/house/balcony.html'],
function(Handlebars, house, storey, section, roofCenter, roofLeft, roofRight, feature, door, window_, balcony) {

  return {
    house: Handlebars.compile(house),
    storey: Handlebars.compile(storey),
    section: Handlebars.compile(section),
    roofCenter: Handlebars.compile(roofCenter),
    roofLeft: Handlebars.compile(roofLeft),
    roofRight: Handlebars.compile(roofRight),
    feature: Handlebars.compile(feature),
    door: Handlebars.compile(door),
    window: Handlebars.compile(window_),
    balcony: Handlebars.compile(balcony)
  };

});

