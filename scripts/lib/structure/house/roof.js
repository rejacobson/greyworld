define(['./templates'], function(Templates) {

  /**
   * Create a roof
   * 
   * @param {integer} width: The max width of the roof
   */
  function create(width, Random) {
    var max_endcap_width = Math.floor(width / 2),
        endcap_width = Random.range(1, max_endcap_width),
        center_width = width - (endcap_width * 2);

    var left_piece = Templates.roofLeft({width: endcap_width}),
        right_piece = Templates.roofRight({width: endcap_width}),
        center_piece = center_width ? Templates.roofCenter({width: center_width}) : '';

    var $storey = $(Templates.storey({width: width, height: endcap_width}));

    return {
      html: $storey.append(left_piece).append(center_piece).append(right_piece),
      width: width,
      height: endcap_width
    };
  };

  return {
    create: create
  };

});

