define(['lib/symmetrical_sort', './templates'], function(symmetricalSort, Templates) {

  /**
   * Create a storey of a house.
   * 
   * @param {integer} floor_number: 1 is the ground floor, 2 is second storey, etc.
   * @param {integer} width: The max width of this floor
   */
  function create(floor_number, width, Random) {
    var sections = [],
        height = 1, //Random.range(1, 2),
        width_remaining = width, 
        roof_sections = [0, 0];

    if (floor_number > 1 && width >= (2 + (height * 2))) {
      // Left Roof section
      if (Random.range(1, 100) > 50) {
        width -= height;
        width_remaining -= height;
        roof_sections[0] = 1;
      }
      
      // Right Roof section
      if (Random.range(1, 100) > 50) {
        width -= height;
        width_remaining -= height;
        roof_sections[1] = 1;
      }
    }   
  
    var num_sections = Random.range(1, width);

    if (width > 4) {
      // Constrain to odd number of sections
      while (num_sections % 2 == 0) {
        num_sections = Random.range(1, width);
      }
    }

    // Pick a size for each section
    for (var i=num_sections-1; i>=0; --i) {
      var min_width = i==0 ? width_remaining : 1,
          max_width = width_remaining - i,
          section_width = Random.range(min_width, max_width);

      sections.push(section_width);
      width_remaining -= section_width;
    }   

    // Symmetrical sort the sections
    sections = symmetricalSort(sections);
    
    ////////////////////////////////////
    // Build the html for this storey 
    ////////////////////////////////////
    var total_width = width,
        left_roof = '',
        right_roof = '';
    
    if (roof_sections[0] || roof_sections[1]) {
      if (roof_sections[0]) {
        left_roof = Templates.roofLeft({width: height});
        total_width += height;
      }
      if (roof_sections[1]) {
        right_roof = Templates.roofRight({width: height});
        total_width += height;
      }
    }
    
    // Build the floor html
    var $storey = $(Templates.storey({width: total_width, height: height, level: floor_number}));
    
    _.each(sections, function(w){
      $storey.append(Templates.section({width: w}));
    });
    
    $storey.prepend(left_roof).append(right_roof);
    
    return {
      html: $storey,
      width: width,
      height: height,
      roofing: [roof_sections[0] || false, roof_sections[1] || false]
    };
  };

  return {
    create: create
  };

});
