define(['underscore'], function(_) {

  //                  [w, h]   [w, h]
  var Grid = function(mapsize, cellsize) {
    // The number of cells in each of the x and y axis
    var cell_count_x = Math.round(mapsize[0] / cellsize[0]),
        cell_count_y = Math.round(mapsize[1] / cellsize[1]);

    // The size of the area being partitioned.
    this.mapsize = mapsize;

    // The number of cells in each of the x and y axis
    this.cellcount = [cell_count_x, cell_count_y];

    // Recalculate the cell sizes based off of the x and y cell counts
    this.cellsize = [mapsize[0] / cell_count_x, mapsize[1] / cell_count_y];

    // The largest cell index in the map
    this.max = this.cellcount[0] * this.cellcount[1] - 1;

    // Keep track of the number of elements in each cell
    // residents[cell_index] == number of elements
    this.residents = {};
    
    // Array of grid cells
    // This holds the entities/values being partitioned
    this.map = [];
  };

  // Add a new element to the grid, in the specified cell index
  Grid.prototype.insert = function(index, value) {
    var cell = this.getCell(index, []);
    cell.push(value);
    if (!this.residents[index]) this.residents[index] = 0;
    this.residents[index]++; // increment the number of elements in this cell
  };

  // Remove an element from the grid and return it
  Grid.prototype.remove = function(index, value) {
    var cell = this.getCell(index);

    for (var i=0; i<cell.length; ++i) {
      if (cell[i] == value) {
        cell.splice(i, 1);
        this.residents[index]--;
        if (this.residents[index] <= 0) delete this.residents[index];
        break;
      }
    }

    return value;
  };

  // Return the stored element counts.
  // Only cells with more than one element in them are represented.
  Grid.prototype.population = function() {
    return this.residents;
  };

  // Get the contents of a cell, or cells, by it's index, setting a default value if the cell is undefined
  Grid.prototype.getCell = function(index, _default) {
    // Return multiple indices
    if (_.isArray(index)) {
      var list = [], cell;
      for (var i=0, len=index.length; i<len; ++i) {
        cell = index[i];
        if (this.map[cell] && this.map[cell].length) {
          list = list.concat(this.map[cell]);
        }
      };
      return list;
    }
    
    if (_default && this.map[index] == undefined) this.map[index] = _default;

    // Return a single index
    return this.map[index];
  };

  // Get the contents of all cells surrounding the cell at index
  Grid.prototype.getSurroundingCells = function(index) {
    return this.getCell(this.getSurroundingIndices(index));
  };

  // Get a list of all indices surrounding the cell at index 
  Grid.prototype.getSurroundingIndices = function(index) {
    var indices = [],
        coords = this.getCoordinatesByIndex(index),
        x = coords[0],
        y = coords[1],
        width = this.cellcount[0],
        height = this.cellcount[1];

    // X is not touching left hand side
    if (x > 1) {
      indices.push(index - 1); // WEST
      
      if (y > 1) indices.push(index - width - 1); // NORTH WEST
      if (y < height - 1) indices.push(index + width - 1); // SOUTH WEST
    }

    // X is not touching the right hand side
    if (x < width - 1) {
      indices.push(index + 1); // EAST
     
      if (y > 1) indices.push(index - width + 1); // NORTH EAST
      if (y < height - 1) indices.push(index + width + 1); // SOUTH EAST
    }

    // Y is not touching the top
    if (y > 1) indices.push(index - width); // NORTH

    // Y is not touching the bottom
    if (y < height - 1) indices.push(index + width); // SOUTH
    
    return indices;
  };

  // Get a cell x and y coordinates by a cell index
  Grid.prototype.getCoordinatesByIndex = function(index) {
    var y = Math.floor(index / this.cellcount[0]),
        x = index - (y * this.cellcount[0]);
    return [x, y];
  };

  // Get the cell index by a position relative to the map size
  Grid.prototype.getIndexByPosition = function(position) {
    var x = Math.floor(position[0] / this.cellsize[0]),
        y = Math.floor(position[1] / this.cellsize[1]);
        
    var index = (y * this.cellcount[0]) + x;

    // Return null if the position is out of bounds
    if (index < 0 || index > this.max) return null;

    return index;
  };

  // Get a cell position relative to the map size by a cell index
  // Returns the position of the top left of the cell
  Grid.prototype.getPositionByIndex = function(index) {
    var coords = this.getCoordinatesByIndex(index);
    return [coords[0] * this.cellsize[0], coords[1] * this.cellsize[1]];
  };

  return Grid;

});
