define(function(){
  
  
  /**
   * @param {Array} origin point [a0, a1, a2]
   * @param {Array} target point [b0, b1, b2]
   * @returns {Number} distance between two points
   */
  var distance = function(a, b) {
     return len(subtract(a, b));
  };

  /**
   * @param {Array} origin point [a0, a1, a2]
   * @param {Array} target point [b0, b1, b2]
   * @returns {Number} distance^2 between two points
   */
  var distance_sq = function(a, b) {
     return len_sq(subtract(a, b));
  };

  /**
   * @param {Array} vector [v0, v1, v2]
   * @returns {Number} length of vector
   */
  var len = function(v) {
    return Math.sqrt(len_sq(v));
  };

  /**
   * @param {Array} vector [v0, v1, v2]
   * @returns {Number} length^2 of vector
   */
  var len_sq = function(v) {
    return v[0]*v[0] + v[1]*v[1] + v[2]*v[2];
  };

  /**
   * subtracts vectors [a0, a1, a2] - [b0, b1, b2]
   * @param {Array} a
   * @param {Array} b
   * @returns {Array} vector
   */
  var subtract = function(a, b) {
     return [a[0] - b[0], 
             a[1] - b[1], 
             a[2] - b[2]];
  };
  
  /**
   * adds vectors [a0, a1, a2] - [b0, b1, b2]
   * @param {Array} a vector
   * @param {Array} b vector
   * @returns {Array} vector
   */
  var add = function(a, b) {
     return [a[0] + b[0], 
             a[1] + b[1],
             a[2] + b[2]];
  };
  
  /**
   * multiply vector with scalar or other vector
   * @param {Array} vector [a0, a1, a2]
   * @param {Number|Array} vector or number
   * @returns {Number|Array} result
   */
  var multiply = function(a, s) {
     if (typeof s === 'number') {
      return [a[0] * s,
              a[1] * s,
              a[2] * s];
     }
  
     return [a[0] * s[0],
             a[1] * s[1],
             a[2] * s[2]];
  };
  
  /**
   * @param {Array} a vector
   * @param {Number} s
   */
  var divide = function(a, s) {
     if (typeof s === 'number') {
      return [a[0] / s, 
              a[1] / s,
              a[2] / s];
     }
     
     return [a[0] / s[0], 
             a[1] / s[1], 
             a[2] / s[2]];
  };
  
  
  /**
   *
   * normalize vector to unit vector
   * @param {Array} vector [v0, v1, v2]
   * @returns {Array} unit vector [v0, v1, v2]
   */
  var unit = function(v) {
     var l = len(v);
     if (l) return [v[0] / l, 
                    v[1] / l, 
                    v[2] / l];
                   
     return [0, 0, 0];
  };
  
  /**
   *
   * calculate vector dot product
   * @param {Array} vector [v0, v1, v2]
   * @param {Array} vector [v0, v1, v2]
   * @returns {Number} dot product of v1 and v2
   */
  var dot = function(v1, v2){
    return (v1[0] * v2[0]) + 
           (v1[1] * v2[1]) + 
           (v1[2] * v2[2]);
  };
  
  /**
   * @returns {Array} vector with max length as specified.
   */
  var truncate = function(v, maxLength) {
    if (len(v) > maxLength) {
      return multiply(unit(v), maxLength);
    };
    return v;
  };
  
  
  return {
    distance: distance,
    distance_sq: distance_sq,
    len: len,
    len_sq: len_sq,
    subtract: subtract,
    add: add,
    multiply: multiply,
    divide: divide,
    unit: unit,
    dot: dot,
    truncate: truncate
  };
  
});
