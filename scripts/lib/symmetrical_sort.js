define(function(){

  /**
   * Sort an array of integers so that the front and rear of the array are mirrored.
   *
   * Eg.  input  ->  [1, 2, 2]
   *      output ->  [2, 1, 2]
   */
  var symmetricalSort = function(array) {
    var source = array.slice(0).sort(),
        new_array = [],
        sample = {};

    for (var i=0, len=source.length; i<len; ++i) {
      var value = source[i];
      if (sample[value] == undefined) sample[value] = {count: 0, original: value};
      sample[value].count++;
    }

    var keys = [];
    var has_even = false;

    var iterator = _.keys(sample);
    for (var i=0, len=iterator.length; i<len; ++i) {
      if (sample[iterator[i]].count % 2 == 0) has_even = true;

      if (has_even) {
        // sort keys, odd numbers go at the beginning, even numbers at the end
        _.each(_.keys(sample).sort().reverse(), function(value) {
          // Even
          if (sample[value].count % 2 == 0)
            keys.push(value);

          // Odd
          else
            keys.unshift(value);
        });

        break;
      }
    }

    if (!keys.length) {
      keys = _.keys(sample).reverse();
    }

    _.each(keys, function(value) {
      var count = sample[value].count,
          original = sample[value].original;

      for (var j=0; j<Math.floor(count/2); ++j) {
        new_array.push(original);
      }
      for (var j=0; j<Math.ceil(count/2); ++j) {
        new_array.unshift(original);
      }
    });

    return new_array;
  } 

  return symmetricalSort;

});
