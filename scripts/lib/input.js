define(['jquery', 'underscore'], function($, _){

  var Keycodes = {
    up: 38,
    down: 40,
    right: 39,
    left: 37,

    space: 32,
    backspace: 8,
    tab: 9,
    enter: 13,
    shift: 16,
    ctrl: 17,
    alt: 18,
    esc: 27,

    0: 48,
    1: 49,
    2: 50,
    3: 51,
    4: 52,
    5: 53,
    6: 54,
    7: 55,
    8: 56,
    9: 57,
    a: 65,
    b: 66,
    c: 67,
    d: 68,
    e: 69,
    f: 70,
    g: 71,
    h: 72,
    i: 73,
    j: 74,
    k: 75,
    l: 76,
    m: 77,
    n: 78,
    o: 79,
    p: 80,
    q: 81,
    r: 82,
    s: 83,
    t: 84,
    u: 85,
    v: 86,
    w: 87,
    x: 88,
    y: 89,
    z: 90,

    kp1: 97,
    kp2: 98,
    kp3: 99,
    kp4: 100,
    kp5: 101,
    kp6: 102,
    kp7: 103,
    kp8: 104,
    kp9: 105
  } 

  /**
    Input.on('keydown.a', function(){
    });
  */

  var Input = (function() {
    var callbacks = {
          'keydown': {},
          'keyup': {}
        },
        keysdown = {},
        keysup = {};
    
    return {  
      on: function(name, callback, context) {
        var parts = name.split('.'),
            type = parts[0],
            key = parts[1],
            code = Keycodes[key];
        
        if (!callbacks[type][code]) callbacks[type][code] = [];
            
        callbacks[type][code].push(function(e){
          callback.call(context, e);
        });

        return this;
      },

      keydown: function(code) {
        if (!code) return;
        if (!keysdown[code]) {
          keysdown[code] = true;
        }
      },
      
      keyup: function(code) {
        if (!code) return;
        delete keysdown[code];
        keysup[code] = true;
      },
      
      update: function(dt) {
        // Run keydown callbacks
        _.each(keysdown, function(value, key) {
          if (value === true && callbacks['keydown'][key] && callbacks['keydown'][key].length) {
            _.each(callbacks['keydown'][key], function(lambda){
              lambda();
            });
          }

          // Mark this keydown as executed so it is not run again
          keysdown[key] = 2;
        });
        
        // Run keyup callbacks
        _.each(keysup, function(value, key) {
          if (callbacks['keyup'][key] && callbacks['keyup'][key].length) {
            _.each(callbacks['keyup'][key], function(lambda){
              lambda();
            });
          }
        });
        
        // Flush the keysup memo on every tick
        keysup = {};
      }
      
    };
  })();

  $(document).on('keydown', function(e){
    Input.keydown(e.keyCode);
  }).on('keyup', function(e) {
    Input.keyup(e.keyCode);
  });

  return Input;

});
