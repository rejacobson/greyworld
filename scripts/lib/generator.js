define(['underscore', 'lib/mersenne', 'lib/terrain'],
function(_, MersenneTwister, Terrain) {

  var populate_scene = function(scene) {
    var x = scene.coords[0],
        y = scene.coords[1],
        seed = parseInt(x +''+ y +''+ (Math.round(values['terrain']['score']) * (values['city'] ? Math.round(values['city']['score']) : 0)));
        Random = new MersenneTwister(seed);

    //////////////////////////////////////
    // Terrain Features
    //////////////////////////////////////
    var biome = Terrain.biome(x, y),
        density = terrain.density;

    scene.html().addClass(terrain.name);

    // 1. Add features to the road
    var count = parseInt(scene.width() / 100 * (density/100)),
        index, feature,
        map = probability_map(Descriptor.features);

    for (var i=0; i<count; ++i) {
      index = Random.range(0, map.length-1);
      feature = map[index]; 
      feature = feature.charAt(0).toUpperCase() + feature.slice(1); // Capitalize
console.log('Creating a '+ feature);
console.log(scene.width() +':'+ scene.road().height());
      if (Entities[feature] && Entities[feature].generate) {
        var entity = Entities[feature].generate(Random);
        entity.position(Random.range(1, scene.width() - 1), Random.range(1, scene.road().height() - 1));
        scene.addEntity(entity);
      }
    }

    // 2. Add decorations to the road


    // 3. Add features to the scenery


    // 4. Set road color and texture

    
    //////////////////////////////////////
    // City Features
    //////////////////////////////////////
    if (values['city']) {
      var city = values['city'],
          score = city.score,
          density = city.density;

      scene.html().addClass('city');

      // Add features to the road

      // Add features to the scenery
    }
  }

  return {
    populate_scene: populate_scene
  };

});
