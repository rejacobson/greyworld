define(['jquery', 'lib/simplex-noise', 'lib/mersenne', 'lib/terrain'], function($, SimplexNoise, MersenneTwister, Terrain) {

  Terrain.initialize();

console.log(Terrain.biome(0, 0));

  //the jquery.alpha.js and jquery.beta.js plugins have been loaded.
  $(function() {

    var max = 80,
        table = $('#terrain'),
        tr, td, score, color,

        elevation_colors = ['#8c0c29', '#8f3f0c', '#8c8706', '#378506', '#509a03', '#3cfe0d', '#214a88'],
        elevation_range = 100 / elevation_colors.length;


    // Generate elevation data

    for (var x = -max; x<=max; x++) {
      tr = $('<tr>');

      for (var y= -max; y<=max; y++) {
        var biome = Terrain.biome(x, y),
            population = Terrain.score('Habitation', x, y);

        td = $('<td>').attr({
          id: '_'+ x +'_'+ y,
          title: 'Coords: '+ x +':'+ y +' -- Biome: '+ biome.name +' -- Density: '+ biome.density +' -- Edge: '+ biome.edge
        }).addClass(biome.name);

        if (population > 84 && biome.name != 'water' && biome.name != 'scorched') {
          td.addClass('city');
          td.attr('title', td.attr('title') + ' -- City: '+ population);
        }

        tr.append(td);
      }

      table.append(tr);
    }

    var $blinking = 'dummy', klass = '';
    function blink_feature(type) {
      klass = type;
      $blinking = $('table .'+ klass);
    }
    function blinker() {
      if (!$blinking.length) return;
      $blinking.toggleClass(klass);
      setTimeout(blinker, $blinking.first().hasClass(klass) ? 1000 : 10);
    }

    blink_feature('city');
    blinker();

/*
    // Generate elevation data
    for (var x = -max; x<=max; x++) {
      tr = $('<tr>');

      for (var y= -max; y<=max; y++) {
        score = Terrain.score('Moisture', x, y);
        for (var c=0, len=elevation_colors.length; c<len; ++c) {
          color = elevation_colors[c];
          if (score > (100 - (c * elevation_range))) break;
        }

        td = $('<td>').attr({
          id: '_'+ x +'_'+ y,
          title: 'Coords: '+ x +':'+ y +' -- Elevation: '+ score
        }).css({backgroundColor: color});

        tr.append(td);
      }

      table.append(tr);
    }
*/


/*
    // Generate elevation data
    for (var x = -max; x<=max; x++) {
      tr = $('<tr>');

      for (var y= -max; y<=max; y++) {
        noise_values = Terrain.values(x, y);

        td = $('<td>').attr({
          id: '_'+ x +'_'+ y,
          title: 'Coords: '+ x +':'+ y +' -- Terrain: '+ noise_values['terrain']['score'] +'/'+ noise_values['terrain']['density'],
          class: noise_values['terrain']['name']
        });

        if (noise_values['city']) {
          td.addClass('city');
          td.attr('title', td.attr('title') +' -- City: '+ noise_values['city']['score'] +'/'+ noise_values['city']['density']);
        }

        if (x==0 && y==0) td.addClass('center');

        tr.append(td);
      }

      table.append(tr);
    }
*/

  });

});
